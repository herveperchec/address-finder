import Vue from 'vue'
import VueRouter from 'vue-router'

// Routes
import publicRoutes from './routes/public'

// Middlewares
import AuthMiddleware from './middlewares/AuthMiddleware'
import AdminMiddleware from './middlewares/AdminMiddleware'
import RequireUsersDataMiddleware from './middlewares/RequireUsersDataMiddleware'

// Use plugin
Vue.use(VueRouter)

// Routes
const routes = [
  ...publicRoutes
]

// Router
const router = new VueRouter({
  routes,
  // To return to page top when navigate
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {
        x: 0,
        y: 0,
        behavior: 'smooth'
      }
    }
  }
})

/* Middlewares */

router.beforeEach(AuthMiddleware) // Auth middleware
router.beforeEach(AdminMiddleware) // Admin middleware
router.beforeEach(RequireUsersDataMiddleware) // RequireUsersData middleware

export default router
