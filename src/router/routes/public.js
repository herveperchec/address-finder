// Components
import Home from '@/views/Home.vue'
import NotFound from '@/views/NotFound.vue'

/**
 * vue-router public routes
 */
export default [
  /**
   * Home
   */
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      breadcrumb: {
        schema: null,
        title: (vm) => 'Accueil'
      }
    }
  },
  /**
   * Catch all
   */
  {
    // will match everything
    path: '*',
    name: 'CatchAll',
    redirect: '404'
  },
  /**
   * 404
   */
  {
    path: '/404',
    name: '404',
    component: NotFound
  }
]
