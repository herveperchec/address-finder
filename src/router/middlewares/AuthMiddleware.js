import Store from '@/store'
/**
 * vue-router 'Auth' middleware
 */
export default async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    const authenticated = await Store.dispatch('Auth/checkAuthenticated')
    if (!authenticated) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
}
