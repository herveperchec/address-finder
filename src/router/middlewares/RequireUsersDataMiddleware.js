import Store from '@/store'
/**
 * vue-router 'RequireUsersData' middleware
 */
export default async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresUsersData)) {
    // this route requires users data to work correctly
    await Store.dispatch('Users/loadUsersData')
    next()
  } else {
    next() // make sure to always call next()!
  }
}
