import Store from '@/store'
/**
 * vue-router 'Admin' middleware
 */
export default async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAdmin)) {
    // this route requires admin role
    // if not, redirect to 404 page.
    const isAdmin = Store.state.Auth.currentUser.isAdmin()
    if (!isAdmin) {
      next({
        path: '/404'
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
}
