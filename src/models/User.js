/**
 * User class
 */
class User {
  /**
   * @private
   */
  _id;
  _firstname;
  _lastname;
  _username;
  _biography;
  _email;
  _birthDate;
  _thumbnail;
  _role;
  _createdAt;
  _updatedAt;
  _lastLogin;
  _authenticated;

  /**
   * Create a User
   * @param {Object} data - Object that represents the user
   * @param {string} data.id - The unique user id
   * @param {string} data.firstname - The user firstname
   * @param {string} data.lastname - The user lastname
   * @param {string} data.username - The user username
   * @param {string} data.email - The user email
   * @param {string} data.biography - The user biography
   * @param {string} data.birth_date - The user birth date
   * @param {boolean} data.thumbnail - The user guild thumbnail
   * @param {string} data.role - The user role
   * @param {string} data.created_at - When the user has been created
   * @param {string} data.updated_at - When the user has been updated
   * @param {?string} data.last_login - User last login date or null
   * @param {boolean} [authenticated = false] - If the user is authenticated (default: false)
   */
  constructor (data, authenticated = false) {
    this._id = data.id
    this._firstname = data.firstname
    this._lastname = data.lastname
    this._username = data.username
    this._email = data.email
    this._biography = data.biography
    this._birthDate = data.birth_date ? new Date(data.birth_date) : null
    this._thumbnail = data.thumbnail
    this._role = data.role
    this._createdAt = new Date(data.created_at)
    this._updatedAt = new Date(data.updated_at)
    this._lastLogin = data.last_login
    this._authenticated = authenticated
  }

  /**
   * id
   * @category properties
   * @readonly
   * @description User id accessor
   * @type {number}
   */
  get id () {
    return this._id
  }

  /**
   * firstname
   * @category properties
   * @readonly
   * @description User firstname accessor
   * @type {string}
   */
  get firstname () {
    return this._firstname
  }

  /**
   * lastname
   * @category properties
   * @readonly
   * @description User lastname accessor
   * @type {string}
   */
  get lastname () {
    return this._lastname
  }

  /**
   * username
   * @category properties
   * @readonly
   * @description User username accessor
   * @type {string}
   */
  get username () {
    return this._username
  }

  /**
   * email
   * @category properties
   * @readonly
   * @description User email accessor
   * @type {string}
   */
  get email () {
    return this._email
  }

  /**
   * biography
   * @category properties
   * @readonly
   * @description User biography accessor
   * @type {string}
   */
  get biography () {
    return this._biography
  }

  /**
   * birthDate
   * @category properties
   * @readonly
   * @description User birthDate accessor
   * @type {Date}
   */
  get birthDate () {
    return this._birthDate
  }

  /**
   * role
   * @category properties
   * @readonly
   * @description User role accessor
   * @type {string}
   */
  get role () {
    return this._role
  }

  /**
   * thumbnail
   * @category properties
   * @readonly
   * @description User thumbnail accessor
   * @type {Blob}
   */
  get thumbnail () {
    return this._thumbnail
  }

  /**
   * createdAt
   * @category properties
   * @readonly
   * @description User createdAt accessor
   * @type {Date}
   */
  get createdAt () {
    return this._createdAt
  }

  /**
   * updatedAt
   * @category properties
   * @readonly
   * @description User updatedAt accessor
   * @type {Date}
   */
  get updatedAt () {
    return this._updatedAt
  }

  /**
   * lastLogin
   * @category properties
   * @readonly
   * @description User lastLogin accessor
   * @type {Object[]}
   */
  get lastLogin () {
    return this._lastLogin
  }

  /**
   * authenticated
   * @category properties
   * @description User authenticated accessor
   * @type {boolean}
   */
  get authenticated () {
    return this._authenticated
  }

  set authenticated (value) {
    this._authenticated = value
  }

  /**
   * fullName
   * @category methods
   * @description Returns concatenated firstname and lastname with a white space
   * @return {string}
   */
  fullName () {
    return this.firstname + ' ' + this.lastname
  }

  /**
   * isAdmin
   * @category methods
   * @description Return true if user is admin
   * @return {boolean}
   */
  isAdmin () {
    return this.role === 'admin'
  }

  /**
   * Default properties for a new empty 'User' object
   * @return {Object} Default properties object
   */
  static defaults () {
    return {
      id: null,
      firstname: '',
      lastname: '',
      username: '',
      email: '',
      birthDate: null,
      role: 'user',
      createdAt: null,
      updatedAt: null,
      authenticated: false
    }
  }
}

export default User
