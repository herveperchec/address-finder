/**
 * @vuepress
 * ---
 * title: I18n
 * headline: "App Internationalization (I18n)"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * i18n
 * @module i18n
 * @description
 * Internationalization: VueI18n (see: [https://kazupon.github.io/vue-i18n/](https://kazupon.github.io/vue-i18n/))
 * ```javascript
 * // Import
 * import I18n from '@/i18n'
 * ```
 */

import Vue from 'vue'
import VueI18n from 'vue-i18n'
// Configuration
import CONFIG from '@/config/config.js'
// Import main App internationalization
import en from './en'
import fr from './fr'

Vue.use(VueI18n)

// Messages
const messages = {
  en,
  fr
}

/**
* I18n
* @alias module:i18n
* @type {VueI18n}
* @vuepress_syntax_block I18n
* @vuepress_syntax_desc Access to i18n instance
* @vuepress_syntax_ctx {root}
* this.$i18n
* @vuepress_syntax_ctx {component}
* this.$i18n
* this.$root.$i18n
* @vuepress_syntax_ctx {any}
* window.__ROOT_INSTANCE__.$i18n
* @example
* // Change locale
* this.$i18n.locale = 'en'
* // Translate
* this.$t('path.to.message') // in a component
* // Learn more about vue-i18n at https://kazupon.github.io/vue-i18n/
*/
const I18n = new VueI18n({
  /**
  * locale
  * @alias module:i18n.locale
  * @type {string}
  * @description VueI18n option: locale.
  * @default fr
  */
  locale: CONFIG.I18N.LOCALE,
  /**
  * fallbackLocale
  * @alias module:i18n.fallbackLocale
  * @type {string}
  * @description VueI18n option: fallbackLocale.
  * @default en
  */
  fallbackLocale: CONFIG.I18N.FALLBACK_LOCALE,
  /**
  * messages
  * @alias module:i18n.messages
  * @type {Object}
  * @description VueI18n option: messages.
  * @property {Object} en - English translations
  * @property {Object} fr - French translations
  */
  messages
})

export default I18n
