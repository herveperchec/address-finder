/*   Global mixin   */

const mixin = {
  computed: {
    // To access in any components:
    // this.GLOBALS -> see main.js
    GLOBALS: function () {
      return this.$root.ROOT_DATA.GLOBALS
    },
    // this.CONFIG -> see main.js
    CONFIG: function () {
      return this.$root.ROOT_DATA.CONFIG
    },
    // this.LANGS -> see main.js
    LANGS: function () {
      return this.$root.ROOT_DATA.LANGS
    }
  }
}

export default mixin
