/*
  Root app script
*/

/**
 * Globals
 */
import GLOBALS from '~/globals.config.json'

/**
 * Config
 */
import CONFIG from '@/config/config.js'

/**
 * Packages
 */
// Vue
import Vue from 'vue'
// iso-i18n
import languages from '@cospired/i18n-iso-languages'
// vue-flag-icon
import FlagIcon from 'vue-flag-icon'
// vue-ripple-directive
import Ripple from 'vue-ripple-directive'
// Font awesome icons
import 'vue-awesome/icons'

/**
 * Plugins
 */
// Store
import Store from '@/store'
// Router
import Router from '@/router'
// I18n
import I18n from '@/i18n'
// Bootstrap-vue
import BootstrapVue from 'bootstrap-vue'

/**
 * Core
 */
// Boot
import boot from '#core/boot'
// APIManager service
import APIManager from '#services/api-manager'
// Helpers
import Helpers from '#services/helpers'
// Logger service
import Logger from '#services/logger'
// EventBus service
import EventBus from '#services/event-bus' // eslint-disable-line

/**
 * Mixins
 */
// Global mixin
import GlobalMixin from '@/mixins/GlobalMixin'

/**
 * Components
 */
// Main App component
import App from '@/components/App'
// loadings
import MainLoading from '@/components/loadings/MainLoading'
import LoadingIcon from '@/components/loadings/LoadingIcon'
// modals
import Modal from '@/components/modals/Modal'
import ErrorModal from '@/components/modals/ErrorModal'
// elements
import CustomButton from '@/components/elements/CustomButton'
import CustomCollapse from '@/components/elements/CustomCollapse'
// user components
import UserThumbnail from '@/components/user/UserThumbnail'
// Vue-awesome
import Icon from 'vue-awesome/components/Icon'

// -- END IMPORTS

/**
 * Vue config
 */
// Hide production message in the web browser console
Vue.config.productionTip = process.env.NODE_ENV === 'development'

/**
 * Plugins utilisation
 */
Vue.use(BootstrapVue)
Vue.use(FlagIcon)

/**
 * Mixins utilisation
 */
Vue.mixin(GlobalMixin)

/**
 * Directives declaration
 */
Ripple.color = 'rgba(255, 255, 255, 0.35)' // Default
Ripple.zIndex = 55 // Default
Vue.directive('ripple', Ripple)

/**
 * Components registration
 */
// loadings
Vue.component('MainLoading', MainLoading)
Vue.component('LoadingIcon', LoadingIcon)
// modals
Vue.component('Modal', Modal)
Vue.component('ErrorModal', ErrorModal)
// elements
Vue.component('CustomButton', CustomButton)
Vue.component('CustomCollapse', CustomCollapse)
// user
Vue.component('UserThumbnail', UserThumbnail)
// Vue-awesome
Vue.component('v-icon', Icon)

// -- END INIT

/**
 * Vue root instance
 */
window.__ROOT_INSTANCE__ = new Vue({
  // Name
  name: '__ROOT_INSTANCE__',
  // Main informations
  el: '#app',
  template: '<App/>',
  components: {
    App // Main App component
  },
  // Data for root instance
  data: function () {
    // Return mapped globals
    return {
      ROOT_DATA: {
        GLOBALS: {
          __UI_VERSION__: GLOBALS.VERSION.CURRENT,
          __APP_NAME__: GLOBALS.APP_NAME,
          ...GLOBALS.THEME
        },
        CONFIG: CONFIG,
        LANGS: languages.langs().map((lang) => {
          return {
            lang: lang,
            name: languages.getName(lang, lang)
          }
        })
      }
    }
  },
  // Methods for root instance
  methods: {
    boot: boot
  },
  // I18n
  i18n: I18n,
  // Store
  store: Store,
  // Router
  router: Router,
  // Services (Vue reactive plugins)
  APIManager: APIManager,
  Helpers: Helpers,
  Logger: Logger,
  // 'created' Vue hook
  async created () {
    // Boot
    await this.boot()
  }
})
