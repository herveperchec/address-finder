module.exports = {
  /**
   * Internationalization
   */
  I18N: {
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    LOCALE: 'fr',
    /**
     * Must be 'fr' or 'en'
     * @default 'fr'
     * @type {string}
     */
    FALLBACK_LOCALE: 'fr'
  },
  /**
   * APIs
   */
  API: {
    /**
     * Server API as Axios Instance options
     * @type {Object}
     */
    ServerAPI: {
      baseURL: '/api',
      allowedMethods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    },
    /**
     * Nominatim API as Axios Instance options
     * @type {Object}
     */
    NominatimAPI: {
      baseURL: 'https://nominatim.openstreetmap.org',
      allowedMethods: ['GET'],
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    }
  },
  /**
   * Authentication
   */
  AUTH: {
    /**
     * oAuth access token identifier (in browser LocalStorage)
     * @default 'app_access_token'
     * @type {string}
     */
    ACCESS_TOKEN_NAME: 'app_access_token',
    /**
     * oAuth refresh token identifier (in browser LocalStorage)
     * @default 'app_refresh_token'
     * @type {string}
     */
    REFRESH_TOKEN_NAME: 'app_refresh_token'
  },
  /**
   * Development options
   */
  DEVELOPMENT: {
    /**
     * Define server API in development mode
     */
    API: {
      ServerAPI: {
        URL: 'http://localhost:8000', // Local server url
        APIBaseURL: 'http://localhost:8000/api'
      }
    }
  }
}
