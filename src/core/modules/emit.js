/**
 * @vuepress
 * ---
 * title: emit
 * headline: "Core module: emit"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Core module: emit
 * @module emit
 * @description
 * Core module: emit
 * ```javascript
 * // Import
 * import emit from '#modules/emit'
 * ```
 */

/**
 * emit
 * @alias module:emit
 * @description emit method: shortcut for EventBus $emit method (same paremeters).
 * @return {boolean} See store Users module
 * @vuepress_syntax_block emit
 * @vuepress_syntax_desc Access to emit method, where `<args>` are EventBus.$emit parameters
 * @vuepress_syntax_ctx {root}
 * this.emit(...<args>)
 * @vuepress_syntax_ctx {component}
 * this.$root.emit(...<args>)
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.emit(...<args>)
 */
export default function emit (...args) {
  return this.$EventBus.$emit(...args)
}
