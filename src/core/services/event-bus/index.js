/**
 * @vuepress
 * ---
 * title: EventBus
 * headline: "Service: EventBus"
 * sidebarDepth: 0 # To disable auto sidebar links
 * prev: false # Disable prev link
 * next: false # Disable prev link
 * ---
 */

/**
 * Service: EventBus
 * @module event-bus
 * @description
 * Service: EventBus
 * ```javascript
 * // Import
 * import EventBus from '#services/event-bus'
 * ```
 */

import Vue from 'vue'

const EventBus = new Vue()

Vue.prototype.$EventBus = EventBus

/**
 * EventBus
 * @alias module:event-bus
 * @type {Vue}
 * @constant
 * @description EventBus service
 * The EventBus is the global event bus of system.
 * All Vue instances have access to this.$EventBus and its properties:
 * - $on
 * - $emit
 * - ...
 * @vuepress_syntax_block EventBus
 * @vuepress_syntax_desc Access to EventBus instance
 * @vuepress_syntax_ctx {root}
 * this.$EventBus
 * @vuepress_syntax_ctx {component}
 * this.$EventBus
 * this.$root.$EventBus
 * @vuepress_syntax_ctx {any}
 * window.__ROOT_INSTANCE__.$EventBus
 */
export default EventBus
