/**
 * Service: Logger
 * @module logger
 * @description
 * Service: Logger
 * ```javascript
 * // Import
 * import Logger from '#services/logger'
 * ```
 */

import Vue from 'vue'
// Classes
import Logger from './Logger'

Vue.use(Logger)

/**
 * Logger
 * @alias module:logger
 * @type {Logger}
 * @constant
 * @description Logger service
 * See also [Logger class]{@link Logger}
 */
export default new Logger()
