// Classes
import ServiceAsPlugin from '#services/ServiceAsPlugin.js'

/**
 * Logger class
 */
class Logger extends ServiceAsPlugin {
  /**
   * @private
   */
  _queue;

  /**
   * Create a new Logger
   */
  constructor () {
    const initVMData = {
      queue: []
    }
    // Call ServiceAsPlugin constructor
    super(true, initVMData)
    this._queue = []
  }

  /**
   * queue
   * @category properties
   * @readonly
   * @return {string[]}
   */
  get queue () {
    return this._vm.queue
  }

  set queue (value) {
    throw new Error('Logger class: Can\'t set "queue" property, use "push" method')
  }

  /**
   * push
   * @category methods
   * @description Push a log in the queue
   * @param {string} type - Type ('info', 'advice', ...)
   * @param {string} message - The message
   * @param {string} [location='ui'] - Where display: 'ui' by default, else 'console'
   * @return {void}
   */
  push (type, message, location = 'ui') {
    // Check location
    if (location === 'console') { // Browser console
      this.consoleLog(type, message)
    } else if (location === 'ui') { // MyStation UI logger
      // Check type
      switch (type) {
        case 'info':
          message = '[INFO] ' + message
          break
        case 'advice':
          message = '[ADVICE] ' + message
          break
        default:
          throw new Error('Logger class: "push" method, unknown type')
      }
      this.queue.push(message)
    } else {
      throw new Error('Logger class: "push" method, unknown type')
    }
  }

  /**
   * consoleLog
   * @category methods
   * @description Log a message in the console
   * @param {string} type - Type ('info', 'advice', ...)
   * @param {string} message - The message
   * @return {void}
   */
  consoleLog (type, message) {
    const baseCSS = `padding: 2px; background-color: ${__THEME_PRIMARY_COLOR__};`
    // Check development mode for specific cases
    let devModeRequired = false
    let messageBadge
    let messageBadgeColor
    let messageColor
    // Check type
    switch (type) {
      case 'info':
        messageBadge = '[INFO]'
        messageBadgeColor = '#129999'
        messageColor = __THEME_PRIMARY_COLOR__
        break
      case 'advice':
        messageBadge = '[ADVICE]'
        messageBadgeColor = '#122299'
        messageColor = __THEME_PRIMARY_COLOR__
        devModeRequired = true
        break
      case 'dev':
        messageBadge = '[DEV]'
        messageBadgeColor = '#AD536A'
        messageColor = '#AD536A'
        devModeRequired = true
        break
      case 'warning':
        messageBadge = '[WARN]'
        messageBadgeColor = '#FFFF00'
        messageColor = __THEME_PRIMARY_COLOR__
        break
      case 'error':
        messageBadge = '[ERROR]'
        messageBadgeColor = '#FF1010'
        messageColor = __THEME_PRIMARY_COLOR__
        break
      default:
        messageBadge = '[LOG]'
        messageBadgeColor = '#FFFF00'
        messageColor = __THEME_PRIMARY_COLOR__
    }
    // Don't log if development mode is required and NODE_ENV !== 'development'
    if (!devModeRequired || (devModeRequired && process.env.NODE_ENV === 'development')) {
      console.log( // eslint-disable-line
        // Full string
        '%cServer' + '%c' + messageBadge + '%c ' + message,
        // CSS for %c1
        baseCSS + ' color: white;',
        // CSS for %c2
        baseCSS + ' color: ' + messageBadgeColor + ';',
        // CSS for %c3
        'padding: 2px; color: ' + messageColor + ';'
      )
    }
  }

  /**
   * Install method for Vue
   * The service used as a plugin must absolutely overwrites this static method
   * @ignore
   * @param {Vue} _Vue - Vue
   * @param {Object} [options={}] - The options of the plugin
   */
  static install (_Vue, options = {}) {
    const Vue = _Vue

    const pluginName = 'Logger'

    // Create a 'reactive' mixin
    Vue.mixin(ServiceAsPlugin.createMixin(true, pluginName))

    // Define the getter on the 'private' property
    Object.defineProperty(Vue.prototype, '$' + pluginName, {
      get () { return this['_' + pluginName] }
    })
  }
}

export default Logger
