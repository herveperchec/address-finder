import camelCase from 'camelcase'
import axios from 'axios'
// Classes
import APIOptions from './APIOptions.js'

/**
* API class
*/
class API {
  /**
  * @private
  */
  _name; _driver; _methodsMap;

  /**
  * Create a new API
  * See also [APIOptions class]{@link APIOptions}
  * @param {APIOptions} apiOptions - An APIOptions object
  * @param {string} apiOptions.name - Name of API
  * @param {string} apiOptions.driver - Driver API
  * @param {Object} apiOptions.options - API options
  */
  constructor (apiOptions) {
    if (apiOptions instanceof APIOptions) {
      this.name = apiOptions.name
      this.setDriver(apiOptions.driver, apiOptions.options)
    } else {
      throw new TypeError('API class : constructor failed. APIOptions object attempted, received : ' + typeof apiOptions)
    }
  }

  /**
  * API name
  * @category properties
  * @return {string} The API name
  */
  get name () {
    return this._name
  }

  set name (value) {
    // Check type
    if (typeof value === 'string') {
      // Assign
      this._name = value
    } else {
      // Else -> throw error
      throw new Error('API class: \'name\' property must be String. ' + typeof value + ' received...')
    }
  }

  /**
  * Get the API driver
  * @category properties
  * @readonly
  * @return {Object} The driver
  */
  get driver () {
    return this._driver
  }

  /**
  * Set the driver for the API
  * @category methods
  * @param {string} name - The driver name
  * @param {Object} options - The driver options/configuration
  * @return {void}
  */
  setDriver (name, options) {
    if (API.supportedDrivers.indexOf(name) > -1) {
      this[camelCase(`init_${name}`)](options)
    }
  }

  /**
  * Map driver methods for requests
  * @category methods
  * @description
  * For each HTTP verb, assign driver method:
  * ```javascript
  * {
  *   GET: methods.GET, // this.request('GET', <url>, [<options>])
  *   POST: methods.POST, // this.request('POST', <url>, [<options>])
  *   PATCH: methods.PATCH, // this.request('PATCH', <url>, [<options>])
  *   PUT: methods.PUT, // this.request('PUT', <url>, [<options>])
  *   DELETE: methods.DELETE, // this.request('DELETE', <url>, [<options>])
  *   OPTIONS: methods.OPTIONS // this.request('OPTIONS', <url>, [<options>])
  * }
  * ```
  * @param {Object} methods - A methods object
  * @return {void}
  */
  mapDriverMethods (methods) {
    const genError = (val) => { throw new Error('API class : unmapped or unsupported driver method "' + val + '"') }
    this._methodsMap = {
      GET: methods.GET || function () { genError('GET') },
      POST: methods.POST || function () { genError('POST') },
      PATCH: methods.PATCH || function () { genError('PATCH') },
      PUT: methods.PUT || function () { genError('PUT') },
      DELETE: methods.DELETE || function () { genError('DELETE') },
      OPTIONS: methods.OPTIONS || function () { genError('OPTIONS') }
    }
  }

  /**
  * Main request method
  * @category methods
  * @param {string} method - The method name (ex: GET, POST...)
  * @param {string} url - The URL to request, without baseURL (Ex: /users)
  * @param {Object} [options={}] - The specific options for the driver
  * @return {Promise} The driver Promise response
  */
  request (method, url, options = {}) {
    // If method exists
    if (Object.keys(this._methodsMap).indexOf(method) > -1) {
      // Dynamically call the corresponding method mapped to the driver equivalent
      return this._methodsMap[method](url, options) // Must return a promise
    } else {
      throw new Error('API class : unrecognized method "' + method + '".')
    }
  }

  /**
  * initAxios
  * @category methods
  * @description Assign an axios instance to this.driver
  * @param {AxiosRequestConfig} options - A AxiosRequestConfig object
  * See axios documentation to see configuration options
  * @return {void}
  */
  initAxios (options) {
    // Assign driver
    this._driver = axios.create(options) // AxiosInstance object
    // Map
    this.mapDriverMethods({
      /**
      * Axios driver GET mapped method
      * @ignore
      * @param {String} url - The URL to request (see request method)
      * @param {Object} options - The payload for the driver
      * @return {Promise} - Must return a Promise
      */
      GET: (url, options) => {
        return this._driver.get(url, options) // axios.get(...) -> Promise
      },
      /**
      * Axios driver POST mapped method
      * @ignore
      * @param {String} url - The URL to request (see request method)
      * @param {Object} options - The payload for the driver
      * @return {Promise} - Must return a Promise
      */
      POST: (url, options) => {
        const { data, ...opts } = options
        return this._driver.post(url, data, opts) // axios.post(...) -> Promise
      },
      /**
      * Axios driver PATCH mapped method
      * @ignore
      * @param {String} url - The URL to request (see request method)
      * @param {Object} options - The payload for the driver
      * @return {Promise} - Must return a Promise
      */
      PATCH: (url, options) => {
        const { data, ...opts } = options
        return this._driver.patch(url, data, opts) // axios.patch(...) -> Promise
      },
      /**
      * Axios driver DELETE mapped method
      * @ignore
      * @param {String} url - The URL to request (see request method)
      * @param {Object} options - The payload for the driver
      * @return {Promise} - Must return a Promise
      */
      DELETE: (url, options) => {
        const { data, ...opts } = options
        // 'data' payload property must be named for delete method
        return this._driver.delete(url, { data: data }, opts) // axios.delete(...) -> Promise
      }
    })
  }

  /**
  * @static
  * @type {Array}
  * @readonly
  * @description
  * Only 'axios' is supported as a driver for the moment
  */
  static get supportedDrivers () {
    return ['axios'] // (No more supported 'drivers' for the moment...)
  }
}

export default API
