/**
 * Service: APIManager
 */

import Vue from 'vue'
// Classes
import API from './API'
import APIOptions from './APIOptions'
import APIManager from './APIManager'
// API configurations
import CONFIG from '@/config/config.js'

const APIConfigs = CONFIG.API
// Init collection
const APICollection = [] // Array of APIOptions

// Loop on API configurations
Object.keys(APIConfigs).forEach((apiName) => {
  // Check if development mode and if dev url is defined
  if (process.env.NODE_ENV === 'development') {
    if (CONFIG.DEVELOPMENT.API[apiName]) {
      APIConfigs[apiName].baseURL = CONFIG.DEVELOPMENT.API[apiName].APIBaseURL
    }
  }
  // Create a new APIOptions
  const options = new APIOptions(apiName, 'axios', APIConfigs[apiName])
  // Create a new API
  const api = new API(options)
  // Push to collection
  APICollection.push(api)
})

Vue.use(APIManager)

/**
* APIManager
*/
export default new APIManager(APICollection)
