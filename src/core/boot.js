// Init function
import init from '#core/init'

/**
 * boot
 * @description Must be called by Root Vue instance in "created" hook
 * @return {void}
 */
export default async function boot () {
  // Inititialize first
  await init.call(this) // pass 'this' to child context with call() method
  // Get user data if authenticated
  await this.auth.check()
}
