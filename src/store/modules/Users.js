/**
 * Store module for users
 */

// I18n
import I18n from '@/i18n'
// Models
import User from '#models/User'
// APIManager
import APIManager from '#services/api-manager/index'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

/**
 * Users
 * @alias module:store/modules/Users
 * @type {Object}
 */
const Users = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Users
   */
  state: {
    /**
     * users
     * @description Users data received from server.
     * Typically, the local users
     * @type {User[]}
     * @default []
     * @example
     * // Access to property
     * Store.state.Users.users
     */
    users: [],
    /**
     * usersLoading
     * @description Indicates if users are loading from API
     * @type {boolean}
     * @default false
     */
    usersLoading: false,
    /**
     * forgotPasswordLoading
     * @description Indicates request for forgot password is loading from API
     * @type {boolean}
     * @default false
     */
    forgotPasswordLoading: false,
    /**
     * resetPasswordLoading
     * @description Indicates request for reset password is loading from API
     * @type {boolean}
     * @default false
     */
    resetPasswordLoading: false,
    /**
     * createUserLoading
     * @description Indicates request for create user is loading from API
     * @type {boolean}
     * @default false
     */
    createUserLoading: false,
    /**
     * updateUserLoading
     * @description Indicates request for update user is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserLoading: false,
    /**
     * updateUserThumbnailLoading
     * @description Indicates request for update user thumbnail is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserThumbnailLoading: false,
    /**
     * updateUserPasswordLoading
     * @description Indicates request for update user password is loading from API
     * @type {boolean}
     * @default false
     */
    updateUserPasswordLoading: false,
    /**
     * deleteUserLoading
     * @description Indicates request for delete user is loading from API
     * @type {boolean}
     * @default false
     */
    deleteUserLoading: false
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Users/<name>']
   */
  getters: {
    /**
     * getUser
     * @description Retrieve a user by id
     * @type {function}
     * @param {number} id - User id
     * @return {(User|undefined)}
     */
    getUser: (state) => (id) => {
      // Find user in state.users
      return state.users.find(user => user.id === id)
    }
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Users/<name>')
   */
  actions: {
    /**
     * loadUsersData
     * @description Load users data from API server.
     * Request `[GET] /api/users` (See [API Server documentation](/guide/system/api-server/)).
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadUsersData: async function ({ commit, dispatch }) {
      // Set usersLoading to true
      commit('SET_USERS_LOADING', true)
      // Get users data
      try {
        const usersResponse = await ServerAPI.request('GET', '/users')
        // Set user in state
        commit('SET_USERS', usersResponse.data)
      } catch (error) {
        commit('SET_USERS_LOADING', false)
        dispatch('throwSystemError', I18n.t('errors.e0011'), { root: true }) // root action
        return error
      }
      // Set usersLoading to false
      commit('SET_USERS_LOADING', false)
      // Return true
      return true
    },
    /**
     * createUser
     * @description Create user method.
     * Request `[POST] /api/users/create`
     * to create a new user.
     * @param {Object} payload - Method payload
     * @param {string} payload.username - User username
     * @param {string} payload.firstname - User firstname
     * @param {string} payload.lastname - User lastname
     * @param {string} payload.password - User password
     * @return {(boolean|Error)} Error or return true
     */
    createUser: async function ({ commit, dispatch }, payload) {
      // Set createUserLoading to true
      commit('SET_CREATE_USER_LOADING', true)
      try {
        await ServerAPI.request('POST', '/users/create', {
          data: payload
        })
      } catch (error) {
        // Set createUserLoading to false
        commit('SET_CREATE_USER_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set createUserLoading to false
      commit('SET_CREATE_USER_LOADING', false)
      // Re-load users data
      await dispatch('loadUsersData')
      return true
    },
    /**
     * updateUser
     * @description Update user method.
     * Request `[POST] /api/users/<userId>/update`
     * to update an user.
     * @param {Object} payload.userId - user id
     * @param {Object} payload.data - Method payload -> user data (without thumbnail) to change following format: 'property': 'value'
     * @return {(boolean|Error)} Error or return true
     */
    updateUser: async function ({ commit, dispatch }, payload) {
      // Set updateUserLoading to true
      commit('SET_UPDATE_USER_LOADING', true)
      try {
        await ServerAPI.request('POST', `/users/${payload.userId}/update`, {
          data: payload.data
        })
      } catch (error) {
        // Set updateUserLoading to false
        commit('SET_UPDATE_USER_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set updateUserLoading to false
      commit('SET_UPDATE_USER_LOADING', false)
      // Re-load users data
      await dispatch('loadUsersData')
      return true
    },
    /**
     * updateUserThumbnail
     * @description Update user thumbnail method.
     * Request `[POST] /api/users/<userId>/updateThumbnail`
     * to update the thumbnail of an user.
     * @param {Object} payload.userId - user id
     * @param {Object} payload.data - Method payload -> formData
     * @return {(boolean|Error)} Error or return true
     */
    updateUserThumbnail: async function ({ commit, dispatch }, payload) {
      // Set updateUserThumbnailLoading to true
      commit('SET_UPDATE_USER_THUMBNAIL_LOADING', true)
      try {
        await ServerAPI.request('POST', `/users/${payload.userId}/update/thumbnail`, {
          data: payload.data,
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        })
      } catch (error) {
        // Set updateUserThumbnailLoading to false
        commit('SET_UPDATE_USER_THUMBNAIL_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set updateUserThumbnailLoading to false
      commit('SET_UPDATE_USER_THUMBNAIL_LOADING', false)
      // Re-load users data
      await dispatch('loadUsersData')
      return true
    },
    /**
     * updateUserPassword
     * @description Update user password method.
     * Request `[POST] /api/users/<userId>/update/password`
     * to update the password of an user.
     * @param {Object} payload.userId - user id
     * @param {Object} payload.data - Method payload
     * @param {string} payload.data.password - New password
     * @param {string} payload.data.password_confirmation - New password confirmation
     * @return {(boolean|Error)} Error or return true
     */
    updateUserPassword: async function ({ commit, dispatch }, payload) {
      // Set updateUserPasswordLoading to true
      commit('SET_UPDATE_USER_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', `/users/${payload.userId}/update/password`, {
          data: payload.data
        })
      } catch (error) {
        // Set updateUserPasswordLoading to false
        commit('SET_UPDATE_USER_PASSWORD_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set updateUserPasswordLoading to false
      commit('SET_UPDATE_USER_PASSWORD_LOADING', false)
      return true
    },
    /**
     * deleteUser
     * @description Delete user method.
     * Request `[POST] /api/users/<userId>/delete`
     * to delete a user.
     * @param {Object} userId - user id
     * @return {(boolean|Error)} Error or return true
     */
    deleteUser: async function ({ commit, dispatch }, userId) {
      // Set deleteUserLoading to true
      commit('SET_DELETE_USER_LOADING', true)
      try {
        await ServerAPI.request('POST', `/users/${userId}/delete`)
      } catch (error) {
        // Set deleteUserLoading to false
        commit('SET_DELETE_USER_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set deleteUserLoading to false
      commit('SET_DELETE_USER_LOADING', false)
      // Re-load users data
      await dispatch('loadUsersData')
      return true
    },
    /**
     * forgotPassword
     * @description Send reset password request with email.
     * Request `[POST] /api/forgot-password`
     * @param {Object} payload - Method payload
     * @param {string} payload.email - User email
     * @return {(boolean|Error)} Error or return true
     */
    forgotPassword: async function ({ commit, dispatch }, payload) {
      // Set forgotPasswordLoading to true
      commit('SET_FORGOT_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', '/forgot-password', {
          data: payload
        })
      } catch (error) {
        // Set forgotPasswordLoading to false
        commit('SET_FORGOT_PASSWORD_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set forgotPasswordLoading to false
      commit('SET_FORGOT_PASSWORD_LOADING', false)
      return true
    },
    /**
     * resetPassword
     * @description Reset user password method.
     * Request `[POST] /api/reset-password`
     * to reset a user password.
     * @param {Object} payload - Method payload
     * @param {string} payload.email - User id
     * @param {string} payload.password - New password
     * @param {string} payload.password_confirmation - New password confirmation
     * @return {(boolean|Error)} Error or return true
     */
    resetPassword: async function ({ commit, dispatch }, payload) {
      // Set resetPasswordLoading to true
      commit('SET_RESET_PASSWORD_LOADING', true)
      try {
        await ServerAPI.request('POST', '/reset-password', {
          data: payload
        })
      } catch (error) {
        // Set resetPasswordLoading to false
        commit('SET_RESET_PASSWORD_LOADING', false)
        // Throw error: e0010
        dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        return error
      }
      // Set resetPasswordLoading to false
      commit('SET_RESET_PASSWORD_LOADING', false)
      return true
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Users/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_USERS
     * @description Mutate state.users
     * @param {Object[]} users - Data received from server
     * @return {void}
     */
    SET_USERS (state, users) {
      state.users = users.map((user) => {
        return new User(user)
      })
    },
    /**
     * SET_USERS_LOADING
     * @description Mutate state.usersLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_USERS_LOADING (state, value) {
      state.usersLoading = value
    },
    /**
     * SET_FORGOT_PASSWORD_LOADING
     * @description Mutate state.forgotPasswordLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_FORGOT_PASSWORD_LOADING (state, value) {
      state.forgotPasswordLoading = value
    },
    /**
     * SET_RESET_PASSWORD_LOADING
     * @description Mutate state.resetPasswordLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_RESET_PASSWORD_LOADING (state, value) {
      state.resetPasswordLoading = value
    },
    /**
     * SET_CREATE_USER_LOADING
     * @description Mutate state.createUserLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CREATE_USER_LOADING (state, value) {
      state.createUserLoading = value
    },
    /**
     * SET_UPDATE_USER_LOADING
     * @description Mutate state.updateUserLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_LOADING (state, value) {
      state.updateUserLoading = value
    },
    /**
     * SET_UPDATE_USER_THUMBNAIL_LOADING
     * @description Mutate state.updateUserThumbnailLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_THUMBNAIL_LOADING (state, value) {
      state.updateUserThumbnailLoading = value
    },
    /**
     * SET_UPDATE_USER_PASSWORD_LOADING
     * @description Mutate state.updateUserPasswordLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_UPDATE_USER_PASSWORD_LOADING (state, value) {
      state.updateUserPasswordLoading = value
    },
    /**
     * SET_DELETE_USER_LOADING
     * @description Mutate state.deleteUserLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_DELETE_USER_LOADING (state, value) {
      state.deleteUserLoading = value
    }
  }
}

export default Users
