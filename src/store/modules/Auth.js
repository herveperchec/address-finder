/**
 * Store module for authentication
 */

// I18n
import I18n from '@/i18n'
// Configuration
import CONFIG from '@/config/config.js'
// Models
import User from '#models/User'
// APIManager
import APIManager from '#services/api-manager/index'

// Define server API
const ServerAPI = APIManager.use('ServerAPI')

/**
 * Auth
 * @alias module:store/modules/Auth
 * @type {Object}
 */
const Auth = {
  /**
   * @private
   */
  namespaced: true,
  /**
   * state
   * @type {Object}
   * @readonly
   * @description Store module state
   * @example
   * // Access to the module state
   * Store.state.Auth
   */
  state: {
    /**
     * currentUser
     * @description Current / Authenticated User
     * @type {User}
     * @default new User()
     */
    currentUser: new User(User.defaults()), // default guest
    /**
     * currentUserLoading
     * @description Indicates if current user is loading from API
     * @type {boolean}
     * @default false
     */
    currentUserLoading: false
  },
  /**
   * getters
   * @type {Object}
   * @readonly
   * @description Store module getters
   * @example
   * // Access to the module getters, where <name> is the getter name
   * Store.getters['Auth/<name>']
   */
  getters: {
    // Nothing here...
  },
  /**
   * actions
   * @type {Object}
   * @protected
   * @description Store module actions
   * @example
   * // Access to the module action, where <name> is the action name
   * Store.dispatch('Users/<name>')
   */
  actions: {
    /**
     * login
     * @description Request `[POST] /api/login` with credentials data.
     * If server side success, save tokens in LocalStorage and dispatch 'checkAuthenticated'
     * @param {Object} credentials - The user credentials
     * @param {string} credentials.username - Username
     * @param {string} credentials.password - Password
     * @return {(boolean|Error)} Return value of 'checkAuthenticated'
     */
    login: async function ({ commit, dispatch }, credentials) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      let authResponse = null
      let accessToken = null
      // let refreshToken = null
      // Request for auth
      try {
        authResponse = await ServerAPI.request('POST', '/login', {
          data: credentials
        })
        accessToken = authResponse.data.access_token
        // refreshToken = authResponse.data.refresh_token
        // Save tokens in localStorage
        localStorage.setItem(CONFIG.AUTH.ACCESS_TOKEN_NAME, accessToken)
        // localStorage.setItem(CONFIG.AUTH.REFRESH_TOKEN_NAME, refreshToken)
      } catch (error) {
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        if (!error.response) {
          // Server error
          dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
        } else {
          if (error.response.status === 400 ||
              error.response.status === 401) {
            return false // Bad credentials
          } else {
            dispatch('throwSystemError', I18n.t('errors.e0010'), { root: true }) // root action
          }
        }
        return error
      }
      // Get user
      return dispatch('checkAuthenticated')
    },
    /**
     * logout
     * @description Remove tokens from the local storage and reset current user
     * @return {void}
     */
    logout: async function ({ commit }) {
      try {
        // Request for logout
        await ServerAPI.request('POST', '/me/logout')
      } catch (error) {
        // TO DO ?...
      }
      // Remove access token
      localStorage.removeItem(CONFIG.AUTH.ACCESS_TOKEN_NAME)
      localStorage.removeItem(CONFIG.AUTH.REFRESH_TOKEN_NAME)
      // Reset currentUser
      commit('RESET_CURRENT_USER')
    },
    /**
     * checkAuthenticated
     * @description Check if user is authenticated.
     * First, check if token is set in the local storage,
     * set authorization header and request for user data from server.
     * Else, call 'logout' and return false.
     * @return {(boolean|Error)} False or return value of 'loadCurrentUserData'
     */
    checkAuthenticated: async function ({ commit, dispatch }) {
      // Retrieve token in localStorage
      const accessToken = localStorage.getItem(CONFIG.AUTH.ACCESS_TOKEN_NAME)
      // const refreshToken = localStorage.getItem('ms_refresh_token')
      // If no access token
      if (!accessToken) {
        return false
      }
      // Set token to headers for each future requests
      dispatch('setAuthorizationHeader', accessToken)
      // Get User data
      return dispatch('loadCurrentUserData')
    },
    /**
     * loadCurrentUserData
     * @description Load current user data from API server.
     * Request `[GET] /api/me` (See [API Server documentation](/guide/system/api-server/)).
     * @return {(boolean|Error)} If success: true, else Error
     */
    loadCurrentUserData: async function ({ commit, dispatch }) {
      // Set currentUserLoading to true
      commit('SET_CURRENT_USER_LOADING', true)
      // Get authenticated user data
      try {
        const userResponse = await ServerAPI.request('GET', '/me')
        // Set user in state
        commit('SET_CURRENT_USER', userResponse.data)
      } catch (error) {
        // Authentication token error
        // Remove token and reset user
        dispatch('logout')
        // Set currentUserLoading to false
        commit('SET_CURRENT_USER_LOADING', false)
        dispatch('throwSystemError', I18n.t('errors.e0021'), { root: true }) // root action
        return error
      }
      // Set currentUserLoading to false
      commit('SET_CURRENT_USER_LOADING', false)
      // Return true
      return true
    },
    /**
     * setAuthorizationHeader
     * @description Set the authorization token header for the API instance (ServerAPI).
     * @param {string} token - The access token
     * @return {void}
     */
    setAuthorizationHeader: function ({ commit }, token) {
      // Set authorization token
      ServerAPI.driver.defaults.headers.common.Authorization = 'Bearer ' + token
    },
    /**
     * resetAuthorizationHeader
     * @description Delete the authorization token header for the API instance (ServerAPI).
     * @return {void}
     */
    resetAuthorizationHeader ({ commit }) {
      // Delete header
      delete ServerAPI.driver.defaults.headers.common.Authorization
    }
  },
  /**
   * mutations
   * @type {Object}
   * @protected
   * @description Store module mutations
   * @example
   * // Dispatch a module mutation, where <mutation_name> is the mutation name
   * Store.commit('Auth/<mutation_name>', [payload])
   */
  mutations: {
    /**
     * SET_CURRENT_USER
     * @description Mutate state.currentUser
     * @param {Object} user - Data received from server
     * @return {void}
     */
    SET_CURRENT_USER (state, user) {
      state.currentUser = new User(user)
      state.currentUser.authenticated = true
    },
    /**
     * RESET_CURRENT_USER
     * @description Mutate state.currentUser
     * @return {void}
     */
    RESET_CURRENT_USER (state) {
      state.currentUser = new User(User.defaults())
    },
    /**
     * SET_CURRENT_USER_LOADING
     * @description Mutate state.currentUserLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_CURRENT_USER_LOADING (state, value) {
      state.currentUserLoading = value
    }
  }
}

export default Auth
