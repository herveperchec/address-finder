import Vue from 'vue'
import Vuex from 'vuex'
// Modules
import Auth from './modules/Auth'
import Users from './modules/Users'
// Plugins
import SystemPlugin from './plugins/SystemPlugin'

Vue.use(Vuex)

const Store = new Vuex.Store({
  /**
  * modules
  * @alias module:store.modules
  * @type {Object}
  * @protected
  * @description Vuex Store option: store modules
  */
  modules: {
    Auth,
    Users
  },
  /**
   * plugins
   * @alias module:store.plugins
   * @type {Object}
   * @protected
   * @description Vuex Store option: store plugins
   */
  plugins: [
    SystemPlugin
  ],
  /**
  * state
  * @alias module:store.state
  * @type {Object}
  * @readonly
  * @description Vuex Store option. See syntax for accessor.
  */
  state: {
    /**
    * system
    * @description System data
    * @type {Object}
    */
    system: {
      /**
       * showMainLoading
       * @description Determine if show main loading
       * @type {boolean}
       * @default false
       */
      showMainLoading: false,
      /**
       * showErrorModal
       * @description Determine if show error modal
       * @type {boolean}
       * @default false
       */
      showErrorModal: false,
      /**
       * errorMessage
       * @description The current error message
       * @type {string}
       * @default null
       */
      errorMessage: null
    }
  },
  /**
  * getters
  * @alias module:store.getters
  * @type {Object}
  * @readonly
  * @description Vuex Store option. See syntax for accessor.
  */
  getters: {
    // Nothing for the moment
  },
  /**
  * actions
  * @alias module:store.actions
  * @type {Object}
  * @protected
  * @description Vuex Store option. See syntax for accessor.
  */
  actions: {
    /**
     * throwSystemError
     * @description Throw error and displays error modal
     * @param {string} [message] - (Optional) Error message
     * @return {void}
     * @example
     * this.$store.dispatch('throwSystemError', 'Unknown error appears')
     */
    throwSystemError: function ({ commit }, message) {
      // Show error modal
      commit('SET_SYSTEM_SHOWERRORMODAL', true)
      // Set error message
      commit('SET_SYSTEM_ERRORMESSAGE', message || 'Erreur inconnue')
    },
    /**
     * clearSystemError
     * @description Reset system error
     * @return {void}
     * @example
     * this.$store.dispatch('clearSystemError')
     */
    clearSystemError: function ({ commit }) {
      // Show error modal
      commit('SET_SYSTEM_SHOWERRORMODAL', false)
    }
  },
  /**
  * mutations
  * @alias module:store.mutations
  * @type {Object}
  * @protected
  * @description Vuex Store option. See syntax for accessor.
  */
  mutations: {
    /**
     * SET_SYSTEM_SHOWMAINLOADING
     * @description Mutate state.showMainLoading
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOWMAINLOADING (state, value) {
      state.system.showMainLoading = value
    },
    /**
     * SET_SYSTEM_SHOWERRORMODAL
     * @description Mutate state.system.showErrorModal
     * @param {boolean} value - True or false
     * @return {void}
     */
    SET_SYSTEM_SHOWERRORMODAL (state, value) {
      state.system.showErrorModal = value
    },
    /**
     * SET_SYSTEM_ERRORMESSAGE
     * @description Mutate state.system.errorMessage
     * @param {string} value - Error message
     * @return {void}
     */
    SET_SYSTEM_ERRORMESSAGE (state, value) {
      state.system.errorMessage = value
    }
  }
})

export default Store
