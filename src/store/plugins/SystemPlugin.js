/**
 * vuex store System Plugin
 */
export default (store) => {
  store.subscribe((mutation, state) => {
    // Main loading observer
    const toMirror = [
      // For Auth user loading observer in state.Auth
      'Auth/SET_CURRENT_USER_LOADING',
      // For each 'Users' resource loading observer state.Users
      'Users/SET_USERS_LOADING',
      'Users/SET_FORGOT_PASSWORD_LOADING',
      'Users/SET_RESET_PASSWORD_LOADING',
      'Users/SET_CREATE_USER_LOADING',
      'Users/SET_UPDATE_USER_LOADING',
      'Users/SET_UPDATE_USER_THUMBNAIL_LOADING',
      'Users/SET_UPDATE_USER_PASSWORD_LOADING',
      'Users/SET_DELETE_USER_LOADING'
    ]
    if (toMirror.indexOf(mutation.type) > -1) {
      store.commit('SET_SYSTEM_SHOWMAINLOADING', mutation.payload)
    }
    // appelé après chaque mutation.
    // Les mutations arrivent au format `{ type, payload }`.
  })
}
