const { THEME } = require('./globals.config.json')

const themeGlobals = function () {
  const tmp = {}
  for (let prop in THEME) {
    tmp[prop] = false;
  }
  return tmp
}

module.exports = {
  root: true,
  globals: {
    ...themeGlobals()
  },
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard',
    'plugin:@intlify/vue-i18n/recommended'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    '@intlify/vue-i18n/no-raw-text': ['error', {
      "ignorePattern": '^[-+#:()&"\'.!?]+$',
      "ignoreText": ['English', 'Français']
    }],
    '@intlify/vue-i18n/no-html-messages': 'error',
    '@intlify/vue-i18n/no-missing-keys': 'error',
    '@intlify/vue-i18n/no-v-html': 'warn'
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ],
  settings: {
    '@intlify/vue-i18n': {
      localeDir: './src/i18n/*.json' // extention is glob formatting!
    },
    'vue-i18n': {
      localeDir: './src/i18n/*.json' // extention is glob formatting!
    }
  }

}
