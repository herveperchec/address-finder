const path = require('path')
const webpack = require('webpack')
const StyleLintPlugin = require('stylelint-webpack-plugin')

const { THEME, VERSION } = require('./globals.config.json')

// JS output subdirectory
const jsOutputPath = 'js'
// CSS output subdirectory
const cssOutputPath = 'css'

// Build Global 'THEME' object for define plugin (webpack 'constants') (see below).
const themeGlobalsForDefinePlugin = function () {
  const tmp = {}
  for (let prop in THEME) {
    tmp[prop] = `'${THEME[prop]}'`
  }
  return tmp
}

// Build Global 'THEME' sass vars definitions
// to inject in all sass files (see below)
const themeGlobalsForSassInjection = function () {
  let str = ''
  for (let prop in THEME) {
    str += `$${prop}: ${THEME[prop]};\n`
  }
  return str
}

module.exports = {
  outputDir: 'www',
  publicPath: './',
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    }
  },
  configureWebpack: {
    // Output
    output: {
      filename: `${jsOutputPath}/[name].${VERSION.CURRENT}.js`,
      chunkFilename: `${jsOutputPath}/[name].${VERSION.CURRENT}.js`
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
        '@': path.resolve(__dirname, 'src'),
        '~': path.resolve(__dirname, ''), // Root directory
        '#styles': path.resolve(__dirname, 'src/scss'),
        '#models': path.resolve(__dirname, 'src/models'),
        '#core': path.resolve(__dirname, 'src/core'),
        '#modules': path.resolve(__dirname, 'src/core/modules'),
        '#services': path.resolve(__dirname, 'src/core/services')
      }
    },
    plugins: [
      // To define some global variables
      new webpack.DefinePlugin({
        ...themeGlobalsForDefinePlugin()
        // '__UI_VERSION__': `'${VERSION.CURRENT}'`
      }),
      // Scss linter
      new StyleLintPlugin({
        files: ['src/**/*.{vue,htm,html,css,sss,less,scss,sass}'],
      })
    ]
  },
  css: {
    extract: {
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: `${cssOutputPath}/[name].${VERSION.CURRENT}.css`,
      chunkFilename: `${cssOutputPath}/[id].${VERSION.CURRENT}.css`
    },
    loaderOptions: {
      scss: {
        // To have access to variables in all CHILD SASS files
        prependData: `
          ${themeGlobalsForSassInjection()}
          @import "~#styles/_variables.scss";
        `
      }
    }
  }
}
