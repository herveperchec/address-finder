<p><img width="250" src="<%= projectUrl %>/raw/master/src/assets/img/screen.png" alt="App logo"></p>

# <%= appName %>

[![app](https://img.shields.io/static/v1?label=&message=<%= encodeURIComponent(appName) %>&color=191919&logo=<%= logoImageUrl %>)](<%= projectUrl %>)
[![app-ui](https://img.shields.io/static/v1?labelColor=3D373A&label=address-finder&message=v<%= version %>&color=191919)](<%= projectUrl %>)
[![pipeline status](<%= projectUrl %>/badges/master/pipeline.svg)](<%= projectUrl %>/commits/master)

> See boilerplate documentation for more details
